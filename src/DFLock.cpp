/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: locking-unlocking mutexes by 
            creation/destruction of the class
   Authors: G. Lehmann, B. Gorini, R. Hauser
   Version 1.0 - 12 December 2007
----------------------------------------*/

#include "DFThreads/DFMutex.h"
#include "DFThreads/DFLock.h"

DFLock::DFLock(pthread_mutex_t *mutex):mutex_(mutex),df_mutex_(0) {
  pthread_mutex_lock(mutex_);
}
DFLock::DFLock(DFMutex * df_mutex):mutex_(0),df_mutex_(df_mutex) {
  try {
    df_mutex_->lock();
  }
  catch(...){}
}
DFLock::~DFLock() {
  if(mutex_)
    pthread_mutex_unlock(mutex_);
  else if(df_mutex_) {
    try {
      df_mutex_->unlock();
    }
    catch(...){}
  }
}
