/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include <ctime>
#include <cerrno>
#include <pthread.h>

#include "DFThreads/DFCreator.h"
#include "DFThreads/DFConditional.h"
#include "DFThreads/DFLock.h"

DFConditional * DFConditional::Create(char * name) {
  DFConditional * obj = DFCreator < DFConditional >::Create(name);
  obj->setName(name);
  return obj;
}

DFConditional * DFConditional::Create() {
  return DFCreator < DFConditional >::Create();
}

int DFConditional::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFConditional >::Destroy(tmp.c_str());
  }
}

DFConditional::DFConditional() : locker_(0) , old_locker_(0) {
  pthread_mutex_init(& (mutex_), 0);
  pthread_cond_init(& (cond_), 0);
}

DFConditional::~DFConditional() {
  pthread_mutex_destroy(& mutex_);
  pthread_cond_destroy(& cond_);
}

void DFConditional::unlock() /* throw(DFConditional::NotLockedByThread) */ {
  if (locker_ == pthread_self()) {
    if (old_locker_ !=0 ) { 
      locker_ = old_locker_;
      old_locker_ = 0;
    }
    else {
      locker_ = 0;
    }
    pthread_mutex_unlock(& mutex_);
  }
  else {
    throw NotLockedByThread();
  }
}

void DFConditional::lock() /* throw(DFConditional::AlreadyLockedByThread) */ {
  if (locker_ != pthread_self()) {
    pthread_mutex_lock(& mutex_);
    old_locker_ = locker_;
    locker_ = pthread_self();
  }
  else {
    throw AlreadyLockedByThread();
  }
}

void DFConditional::wait() {
  bool doLock = (locker_ != pthread_self());
  if (doLock) {
    DFLock lock(&mutex_);
    pthread_cond_wait(& cond_, & mutex_);
  }
  else {
    pthread_cond_wait(& cond_, & mutex_);
  }
}

void DFConditional::wait(long int timeout) /* throw(DFConditional::Timeout) */ {
  bool doLock = (locker_ != pthread_self());
  if (doLock) {
    DFLock lock(& mutex_);
    struct timespec timeLimit = { time(0)+timeout , 0 };
    if (pthread_cond_timedwait(& cond_, & mutex_, & timeLimit) == ETIMEDOUT) {
      throw Timeout();
    };
  }
  else {
    struct timespec timeLimit = { time(0)+timeout , 0 };
    if (pthread_cond_timedwait(& cond_, & mutex_, & timeLimit) == ETIMEDOUT) {
      throw Timeout();
    };
  }
}

void DFConditional::broadcast() {
  pthread_cond_broadcast(& cond_);
}

void DFConditional::signal() {
  pthread_cond_signal(& cond_);
}

