/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include <cerrno>
#include <iostream>
#include "DFThreads/DFCreator.h"
#include "DFThreads/DFLock.h"
#include "DFThreads/DFSemaphore.h"

DFSemaphore * DFSemaphore::Create(char * name) {
  DFSemaphore * obj = DFCreator < DFSemaphore >::Create(name);
  obj->setName(name);
  return obj;
}

DFSemaphore * DFSemaphore::Create() {
  return DFCreator < DFSemaphore >::Create();
}

int DFSemaphore::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFSemaphore >::Destroy(tmp.c_str());
  }
}

DFSemaphore::DFSemaphore() {
  value_ = 0;
  pthread_mutex_init(& (mutex_), 0);
  pthread_cond_init(& (cond_), 0);
}

DFSemaphore::~DFSemaphore() {
  pthread_mutex_destroy(& mutex_);
  pthread_cond_destroy(& cond_);
}

int DFSemaphore::up() {
  int myValue;
  DFLock lock(& mutex_);
  value_++;
  myValue = value_;
  pthread_cond_signal(& cond_);
  return myValue;

}

void DFSemaphore::down() {
  DFLock lock(& mutex_);
  while (value_ <= 0) {
    pthread_cond_wait(& cond_, & mutex_);
  }
  --value_;
}

void DFSemaphore::down(long int timeout) /* throw( DFSemaphore::Timeout ) */ {
  DFLock lock(& mutex_);
  struct timespec timeLimit = { time(0)+timeout , 0 };

  while (value_ <= 0) {
    if (pthread_cond_timedwait(& cond_, & mutex_, & timeLimit) == ETIMEDOUT) {
      throw Timeout();
    }
  }
  --value_;
}

int DFSemaphore::decrement() {
  int myValue;
  DFLock lock(& mutex_);
  --value_;
  myValue = value_;
  return myValue;
}

int DFSemaphore::value() {
  int myValue;
  DFLock lock(& mutex_);
  myValue = value_;
  return myValue;
}

