/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include "DFThreads/DFMutex.h"

template class DFCreator < DFMutex >;

DFMutex::DFMutex() : locker_(0) {
  pthread_mutex_init(& (mutex_), 0);
}

DFMutex::~DFMutex() {
  pthread_mutex_destroy(& (mutex_));
}

DFMutex * DFMutex::Create(char * name) {
  DFMutex * obj = DFCreator < DFMutex >::Create(name);
  obj->setName(name);
  return obj;
}

DFMutex * DFMutex::Create() {
  return DFCreator < DFMutex >::Create();
}

int DFMutex::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFMutex >::Destroy(tmp.c_str());
  }
}

void DFMutex::unlock() /* throw( DFMutex::NotLockedByThread ) */ {
  if (locker_ == pthread_self()) {
    locker_ = 0;
    pthread_mutex_unlock(& mutex_);
  }
  else {
    throw NotLockedByThread();
  }
}

void DFMutex::lock() /* throw( DFMutex::AlreadyLockedByThread ) */ {
  if (locker_ != pthread_self()) {
    pthread_mutex_lock(& mutex_);
    locker_ = pthread_self();
  }
  else {
    throw AlreadyLockedByThread();
  }
}

bool DFMutex::trylock() /* throw( DFMutex::AlreadyLockedByThread ) */ {
  bool retval=false;
  if (locker_ != pthread_self()) {
    int rc = pthread_mutex_trylock(& mutex_);
    if (rc == 0) {
      locker_ = pthread_self();
      retval = true;
    }  // FIXME Here we should add more logic 
       // to decode rcs different from EBUSY and throw exceptions  
  }
  else {
    throw AlreadyLockedByThread();
  }
  return retval;
}

