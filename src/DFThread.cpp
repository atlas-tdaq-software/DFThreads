/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G-Lehmann, B.Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include <cerrno>
#include <iostream>

#include "DFThreads/DFThread.h"
#include "DFThreads/DFLock.h"

DFThread::DFThread() : condition_(UNSTARTED), threadsWaiting_(0) {
  pthread_mutex_init(& mutex_, 0);
  pthread_mutex_init(& cond_mutex_, 0);
  pthread_cond_init(& cond_, 0);
  pthread_attr_init(& attributes_);

  /* Set thread attributes */

  pthread_attr_setdetachstate(& attributes_, PTHREAD_CREATE_DETACHED);
}

DFThread::~DFThread() {
  stopExecution();
  pthread_mutex_destroy(& mutex_);
  pthread_mutex_destroy(& cond_mutex_);
  pthread_cond_destroy(& cond_);
}

int DFThread::startExecution() {
  int rc;
  DFLock lock(&mutex_);
  if (condition_ == RUNNING) {
    rc = 1;
  }
  else if ((rc = pthread_create(& id_, & attributes_, EntryPoint,
     this)) == 0) {
       setCondition(RUNNING);
  }
  return rc;
}

void DFThread::stopExecution() {
  DFLock lock(&mutex_);
  if (condition_ == RUNNING) {
    setCondition(CANCEL_REQUESTED);
    if (id_ == pthread_self()) {
      pthread_exit(0);
    }
    else {
      pthread_cancel(id_);
    }
  }
}

void DFThread::setCondition(DFThread::Condition condition) {
  DFLock lock(&cond_mutex_);
  condition_ = condition;
  pthread_cond_broadcast(& cond_);
}

void DFThread::waitForCondition(DFThread::Condition condition) {
  DFLock lock(&cond_mutex_);
  while (condition_ < condition) {
    pthread_cond_wait(& cond_, & cond_mutex_);
  }
}

void DFThread::waitForCondition(DFThread::Condition condition, long int timeout) {
   /* throw(DFThread::Timeout) */ 
     DFLock lock(&cond_mutex_);
     struct timespec timeLimit = { time(0)+timeout , 0 };

     while (condition_ < condition) {
       if (pthread_cond_timedwait(& cond_, & cond_mutex_, & timeLimit) ==
          ETIMEDOUT) {
            throw Timeout();
       }
     }
}

void * DFThread::EntryPoint(void * pthis) {
  /* Update cleanup stack */

  pthread_cleanup_push(DFThread::Cleanup, pthis);

  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, 0);
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);

  /* Run the user code */

  try {
    ((DFThread *) pthis)->run();
  } 
  catch (std::exception &e) {
    (*handler_)(e) ;
  }

  pthread_cleanup_pop(1);

  return (0);
}

void DFThread::Cleanup(void * pthis) {
  DFLock lock(&(((DFThread *) pthis)->mutex_));
  ((DFThread *) pthis)->setCondition(CLEANING_UP);
  ((DFThread *) pthis)->cleanup();
  ((DFThread *) pthis)->setCondition(TERMINATED);
}

void DFThread::default_exception_handler( std::exception &e ) {
  std::cerr << "Thread " << id() 
       << " interrupted by exception: " << std::endl 
       << "  " << e.what() << std::endl ;
}

DFThread::exception_handler DFThread::handler_ = DFThread::default_exception_handler;	

DFThread::exception_handler DFThread::set_exception_handler (DFThread::exception_handler handler)
{
  exception_handler rc = handler_ ;
  handler_ = handler ;
  return rc;
}

DFThread::Key DFThread::createKey(void (*destructor)(void *value)) {
  Key *keyp = new Key; 
  pthread_key_create(keyp,destructor);
  return *keyp;
}

void DFThread::setSpecificObject(DFThread::Key &key,const void *object) {
  pthread_setspecific(key,object);
} 

void * DFThread::getSpecificObject(DFThread::Key &key) {
  return pthread_getspecific(key);
}


