/*
    DFThread package
    Class: ThreadSwitchCond
    Authors: J.Petersen
*/

#ifndef THREADSWITCHCOND_H
#define THREADSWITCHCOND_H

#include "DFThreads/DFThread.h"

class ThreadSwitchCond : public DFThread {
public:
  ThreadSwitchCond(int serialNum, int repeat);
protected:
  virtual void run();
  virtual void cleanup();
private:
  //Distinguish different threads
  int serialNum_;
  //Number of times this thread will yield == # events
  int repeat_;

};

#endif //THREADSWITCHCOND_H
