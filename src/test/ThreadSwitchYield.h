/*
    DFThread package
    Class: ThreadSwitchYield
    Authors: J.Petersen
*/

#ifndef THREADSWITCHYIELD_H
#define THREADSWITCHYIELD_H

#include "DFThreads/DFThread.h"

class ThreadSwitchYield : public DFThread {
public:
  ThreadSwitchYield(int serialNum, int repeat);
protected:
  virtual void run();
  virtual void cleanup();
private:
  //Distinguish different threads
  int serialNum_;
  //Number of times this thread will yield == # events
  int repeat_;

};

#endif //THREADSWITCHYIELD_H
