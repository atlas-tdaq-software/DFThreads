#include <iostream>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

#include "ThreadCmem.h"

#include "rcc_time_stamp/tstamp.h"

using namespace std;

int main (int argc, char* argv[])
{

  int nth, noEvents;
  DFThread * thr[12];

  int runTime=10;

  if ((argc==2) && (isdigit(argv[1][0]))) {
     runTime=strtol(argv[1], 0, 10);
  }
  else if (argc != 1) {
     cerr << "Usage: " << argv[0]
          << " <ss>\nwhere <ss> is the time to run in seconds\n";
     exit (0);
  }

   TS_OPEN(100000, TS_H1);

  cout << " # threads = "; 
  cin >> nth;

  cout << " # events = "; 
  cin >> noEvents;

   TS_START(TS_H1);

//Create the test threads and start their execution
  for (int i = 0; i < nth; i++) {
    thr[i] = new ThreadCmem(i,noEvents);
    thr[i]->startExecution();
  }

  //Give some time to the threads
  sleep(runTime);

   TS_SAVE(TS_H1, "ROS_timing");

   TS_CLOSE(TS_H1);


  //When all the threads have sent their last message wait for them
  //to terminate cleanly
  for (int i = 0; i < nth; i++) {
    cout << "MAIN: waiting for termination of thread " << i << endl;
    thr[i]->stopExecution();
    try {
      thr[i]->waitForCondition(DFThread::TERMINATED, 100);
      cout << "MAIN: thread " << i << " terminated" << endl;
    }
    catch (DFThread::Timeout& ) {
      cout << "MAIN: thread " << i << " did not terminate in time" << endl;
    }
  }

  cout << "MAIN: I am finished with the threads" << endl;

}


