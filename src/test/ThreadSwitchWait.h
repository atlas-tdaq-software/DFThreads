/*
    DFThread package
    Class: ThreadSwitchWait
    Authors: J.Petersen
*/

#ifndef THREADSWITCHWAIT_H
#define THREADSWITCHWAIT_H

#include "DFThreads/DFThread.h"
#include "DFThreads/DFFastMutex.h"
#include "DFThreads/DFConditional.h"

class ThreadSwitchWait : public DFThread {
public:
  ThreadSwitchWait(int serialNum, int repeat);
protected:
  virtual void run();
  virtual void cleanup();
private:
  //Distinguish different threads
  int serialNum_;
  //Number of times this thread will yield == # events
  int repeat_;

};

#endif //THREADSWITCHWAIT_H
