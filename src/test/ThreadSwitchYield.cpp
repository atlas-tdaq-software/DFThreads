/************************************************************************/
/*									*/
/*  file : ThreadSwitchYield.cpp					*/
/*									*/
/* measure the timing of thread switching based on yield()		*/
/*									*/
/* 021018  J.Petersen EP						*/
/*									*/
/* results: with libraries & test compiuled with -O0			*/
/*									*/
/* each yield produces a context switch ( no fall through)		*/
/* (100,201) : 600 ns !							*/
/* (100,200) : 1300 ns round trip					*/
/*									*/
/************************************************************************/

#include <iostream>

#include "rcc_time_stamp/tstamp.h"

#include "ThreadSwitchYield.h"

/*
  Constructor of the test thread
*/

ThreadSwitchYield::ThreadSwitchYield(int serialNum, int repeat) : serialNum_(serialNum),
   repeat_(repeat) {
}

/*
  Body of the test thread (simply yield)
*/

void ThreadSwitchYield::run() {

  std::cout << "Started thread: " << DFThread::id() << std::endl ;

  for (int i = 0; i < repeat_; i++) {

//    cout << " thread # " << serialNum_ << " event # " << i << endl;

    TS_RECORD(TS_H1,100 + serialNum_);
    DFThread::yield();
    TS_RECORD(TS_H1,200 + serialNum_);
  }

}

/*
  Cleanup procedure for the test thread
*/

void ThreadSwitchYield::cleanup() {
}

