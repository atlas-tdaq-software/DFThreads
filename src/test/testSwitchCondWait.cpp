/********************************************************************************/
/*										*/
/*   file testSwitchCondWait.cpp						*/
/*										*/
/* timing of mutexes, condition signalling, thread context switching		*/
/*										*/
/* the code implements an example of condition variable signalling & waiting as */
/* found in several textbooks on pthreads e.g Butenhof & Nichols		*/
/*										*/
/* it implies two context switches: see Butenhof p.82 !!			*/
/*										*/
/* 021015 J.O.Petersen EP							*/
/*										*/
/* results with libraries & test compiled with -O0:				*/
/*										*/
/* (211,120) - signal to after wait peak @ ~ 10 us.				*/
/* with -O0 and time stamps, but still very slow				*/
/* (110,190) - complete loop ~ 11 us						*/
/* (100,110) - get mutex ~ 175 ns (with -O0)					*/
/********************************************************************************/

#include <iostream>
#include <unistd.h>

#include "ThreadSwitchCond.h"
#include "ThreadSwitchWait.h"

#include "rcc_time_stamp/tstamp.h"

using namespace std;

// globals ..

  pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

  int done = 1;		// sync flag

int main() {
  int nth, noEvents;
  DFThread * thr[2];

  TS_OPEN(100000, TS_H1);

  nth = 2;

  cout << " # events = "; 
  cin >> noEvents;

   TS_START(TS_H1);

//Create the test threads and start their execution
  thr[0] = new ThreadSwitchWait(0,noEvents);
  thr[1] = new ThreadSwitchCond(1,noEvents);
  thr[0]->startExecution();
  thr[1]->startExecution();

  //Give some time to the threads to  finish
  sleep(10);

  //When all the threads have finished  wait for them
  //to terminate cleanly
  for (int i = 0; i < nth; i++) {
    cout << "MAIN: waiting for termination of thread " << i << endl;
    thr[i]->stopExecution();
    try {
      thr[i]->waitForCondition(DFThread::TERMINATED, 100);
      cout << "MAIN: thread " << i << " terminated" << endl;
    }
    catch (DFThread::Timeout&) {
      cout << "MAIN: thread " << i << " did not terminate in time" << endl;
    }
  }

  cout << "MAIN: I am finished with the threads" << endl;

   TS_SAVE(TS_H1, "ROS_timing");

   TS_CLOSE(TS_H1);

}

ThreadSwitchWait::ThreadSwitchWait(int serialNum, int repeat) : serialNum_(serialNum),
   repeat_(repeat) {

}

/*
   wait for condition variable
*/

void ThreadSwitchWait::run() {

  int noWaits = 0;

  cout << "Started thread: " << DFThread::id() << endl ;

  for (int i = 0; i < repeat_; i++) {

    TS_RECORD(TS_H1,100 + serialNum_);

    pthread_mutex_lock(&mutex);

    TS_RECORD(TS_H1,110 + serialNum_);

    while ( done == 1) {	// spurious interrupts
      pthread_cond_wait(&cond, &mutex);
      noWaits++;
    }
    done = 1;
    TS_RECORD(TS_H1,120 + serialNum_);
    pthread_mutex_unlock(&mutex);

    TS_RECORD(TS_H1,190 + serialNum_);

  }

  cout << " Number of Waits = " << noWaits << endl;

  return;
}

/*
  Cleanup procedure for the test thread
*/

void ThreadSwitchWait::cleanup() {
}

ThreadSwitchCond::ThreadSwitchCond(int serialNum, int repeat) : serialNum_(serialNum),
   repeat_(repeat) {
}

/*
   signal condition variable
*/

void ThreadSwitchCond::run() {

  int noSignals = 0;
  int noMissed = 0;

  cout << "Started thread: " << DFThread::id() << endl ;

  do {

    TS_RECORD(TS_H1,200 + serialNum_);

    pthread_mutex_lock(&mutex);
    done = 0;
//    pthread_mutex_unlock(&mutex);
    TS_RECORD(TS_H1,210 + serialNum_);
    pthread_cond_signal(&cond);		// no context swicth because of mutex
    noSignals++;
    TS_RECORD(TS_H1,220 + serialNum_);
//    pthread_mutex_lock(&mutex);
    pthread_mutex_unlock(&mutex);	// context switch here 

    if ( done == 0) noMissed++;		// expect done = 1 here !

    while (done == 0 ) {	// wait for ACK
    }

    TS_RECORD(TS_H1,290 + serialNum_);

  } while ( noSignals < repeat_);

  cout << " Number of condition variable signals = " << noSignals << endl;
  cout << " Number of missed context switches = " << noMissed << endl;

  return;
}
/*
  Cleanup procedure for the test thread
*/

void ThreadSwitchCond::cleanup() {
}

