/*
    DFThread package
    Class: MessageExample
    Authors: B.Gorini, G.Lehmann
*/

#ifndef MESSAGEEXAMPLE_H
#define MESSAGEEXAMPLE_H

#include <string>

/*
  Extended string class to be used to hold thread messages text.
*/

class MessageExample : public std::string {
public:  
  virtual ~MessageExample();  
  MessageExample(int threadId, int step, char * priority);
};
#endif //MESSAGEEXAMPLE_H
