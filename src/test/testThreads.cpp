#include <iostream>
#include <unistd.h>

#include "DFThreads/DFInputQueue.h"
#include "DFThreads/DFStandardQueue.h"
#include "DFThreads/DFFastQueue.h"
#include "DFThreads/DFFastBlockingQueue.h"
#include "DFThreads/DFFastNonBlockingQueue.h"
#include "DFThreads/DFStandardPrioritizedQueue.h"

#include "MessageExample.h"
#include "ThreadExample.h"

using namespace std;

//New exception handler
void testHandler( std::exception &e) {
  cout << "THREAD: " << DFThread::id() 
       << " terminated by an exception: " 
       << endl
       << e.what() << endl ;
}

int main() {
  const int nth = 10;
  DFThread * thr[12];

  //Get a pointer to the message queue that will be used to get
  //messages from threads
  DFInputQueue < DFCountedPointer < MessageExample > > * messageQueue = 
    DFStandardQueue < DFCountedPointer < MessageExample > >::Create(const_cast<char*>("Messages"));
 
  //Redefine threads exception handler
  DFThread::set_exception_handler(testHandler) ;

  //Create the test threads and start their execution
  for (int i = 0; i < nth; i++) {
    bool terminateWithException = false ;
    if (i==0) terminateWithException = true ;
    thr[i] = new ThreadExample(i, i, terminateWithException);
    thr[i]->startExecution();
  }

  //Give some time to the threads to send all of their messages
  sleep(10);

  //Wait for the messages from the threads and print them
  bool loopFlag = true ;
  while (loopFlag) {
    try {
      DFCountedPointer < MessageExample > message = messageQueue->pop(10);
      cout << "MAIN: Got message from thread: " << message->c_str() << endl;
    }
    catch (DFInputQueue < DFCountedPointer < MessageExample > >::Timeout&) {
      //If no message arrived in the last 10 seconds, assume that there
      //are is no message leftover and stop waiting for new ones
      loopFlag = false ;
    }
    catch (...) {
      cout << "unexpected exception" << endl ;
    }
  }

  //When all the threads have sent their last message wait for them
  //to terminate cleanly
  for (int i = 0; i < nth; i++) {
    cout << "MAIN: waiting for termination of thread " << i << endl;
    thr[i]->stopExecution();
    try {
      thr[i]->waitForCondition(DFThread::TERMINATED, 100);
      cout << "MAIN: thread " << i << " terminated" << endl;
      delete thr[i];
    }
    catch (DFThread::Timeout&) {
      cout << "MAIN: thread " << i << " did not terminate in time" << endl;
    }
  }

  cout << "MAIN: I am finished with the threads" << endl;

  //Release the queue used to comunicate with threads
  int i = messageQueue->destroy();

  if (i == 0) {
    cout << "MAIN: Input queue cleaned up" << endl;
  }
  else {
    cout << "MAIN: ERROR: Input queue is still referred by some thread" << endl;
  }
}


