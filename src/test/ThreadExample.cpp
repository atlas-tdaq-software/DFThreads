/*
    DFThread package
    Class: ThreadExample
    Authors: B.Gorini, G.Lehmann
*/

#include <unistd.h>
#include <exception>
#include <iostream>

#include "ThreadExample.h"
#include "DFThreads/DFStandardQueue.h"
#include "DFThreads/DFFastQueue.h"
#include "DFThreads/DFFastBlockingQueue.h"
#include "DFThreads/DFFastNonBlockingQueue.h"
#include "DFThreads/DFStandardPrioritizedQueue.h"

/*
  Constructor of the test thread
*/

ThreadExample::ThreadExample(int serialNum, int repeat, bool terminateWithException) 
  : serialNum_(serialNum),
    repeat_(repeat),
    terminateWithException_(terminateWithException)
{
  messageQueue_ =  DFStandardQueue < DFCountedPointer < MessageExample > >::Create(const_cast<char*>("Messages"));
}

/*
  Body of the test thread (simply send messages to the main thread)
*/

void ThreadExample::run() {
	std::cout << "Started thread: " << DFThread::id() << std::endl ;

  for (int i = 0; i < repeat_; i++) {
    sleep(1);

//    //Create message that will be sent with LOW priority
//    DFCountedPointer<MessageExample> 
//      messageLow(new MessageExample(serialNum_,i,"LOW"));
    //Create message that will be sent with NORMAL priority
    DFCountedPointer<MessageExample> 
      messageNormal(new MessageExample(serialNum_,i,const_cast<char*>("NORMAL")));
//    //Create message that will be sent with HIGH priority
//    DFCountedPointer<MessageExample> 
//      messageHigh(new MessageExample(serialNum_,i,"HIGH"));

//    //Send message to the main thread with LOW priority
//    messageQueue_->push(messageLow,
//       DFPrioritizedOutputQueue < DFCountedPointer < MessageExample > >::LOW);
    //Send message to the main thread with HIGH priority
    messageQueue_->push(messageNormal);
//    //Send message to the main thread with HIGH priority
//    messageQueue_->push(messageHigh,
//       DFPrioritizedOutputQueue < DFCountedPointer < MessageExample > >::HIGH);

  }
  if (terminateWithException_) {
    std::cerr << "I am now throwing the exception" << std::endl ;
    throw (testException("This is for testing the handler")) ;
  }
  else {
    while (1) {
      //The thread has finished sending messages => wait for cancellation
      //This is only for testing cancellation mechanism: Normal threads
      //would simply exit
      cancellationPoint();
    }
  }
}

/*
  Cleanup procedure for the test thread
*/

void ThreadExample::cleanup() {
  std::cout << "THREAD " << serialNum_ << ": unlinking from queue" << std::endl;
  //Release the queue used to comunicate with the main thread
  int i = messageQueue_->destroy();
  std::cout << "THREAD " << serialNum_ << ": I haved cleaned up: remaining " << i <<
     " links to the message queue" << std::endl;
}

