/*
    DFThread package
    Class: ThreadExample
    Authors: B.Gorini, G.Lehmann
*/

#ifndef THREADEXAMPLE_H
#define THREADEXAMPLE_H

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFThread.h"
#include "MessageExample.h"
#include <exception>

class testException : public std::exception {
public:
  testException(const char * message) : message_(message) { }
  const char * what() const throw () { return message_ ; }
private:
  const char * message_ ;
};


class ThreadExample : public DFThread {
public:
  typedef DFOutputQueue < DFCountedPointer < MessageExample > > messageQueue_t ;
  ThreadExample(int serialNum, int repeat, bool terminateWithException=false);
protected:
  virtual void run();
  virtual void cleanup();
private:
  //Distinguish different threads
  int serialNum_;
  //Number of times this thread will send messages to the main thread
  int repeat_;
  // define if the thread should terminate throwing an exception!
  bool terminateWithException_ ;
  //Communication queue (using counted pointers to store info, provides
  //automatic garbage collection)
  messageQueue_t * messageQueue_;
};

#endif //THREADEXAMPLE_H
