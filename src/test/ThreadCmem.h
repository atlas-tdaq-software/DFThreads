/*
    DFThread package
    Class: ThreadCmem
    Authors: J.Petersen
*/

#ifndef THREADCMEM_H
#define THREADCMEM_H

#include "DFThreads/DFThread.h"

class ThreadCmem: public DFThread {
public:
  ThreadCmem(int serialNum, int repeat);
protected:
  virtual void run();
  virtual void cleanup();
private:
  //Distinguish different threads
  int serialNum_;
  //Number of times this thread will yield == # events
  int repeat_;

};

#endif //THREADCMEM_H
