/************************************************************************/
/*									*/
/*  file : ThreadCmem.cpp					*/
/*									*/
/* measure the timing of thread switching based on yield()		*/
/*									*/
/* 021018  J.Petersen EP						*/
/*									*/
/* results: with libraries & test compiuled with -O0			*/
/*									*/
/* each yield produces a context switch ( no fall through)		*/
/* (100,201) : 600 ns !							*/
/* (100,200) : 1300 ns round trip					*/
/*									*/
/************************************************************************/

#include <iostream>
#include <unistd.h>

#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"

#include "rcc_time_stamp/tstamp.h"

#include "ThreadCmem.h"

/*
  Constructor of the test thread
*/

ThreadCmem::ThreadCmem(int serialNum, int repeat) : serialNum_(serialNum),
   repeat_(repeat) {
}

/*
  Body of the test thread (simply yield)
*/

void ThreadCmem::run() {

  int handle;

  std::cout << "Started thread: " << DFThread::id() << std::endl ;

  unsigned int ret = CMEM_Open();
  if (ret)
    rcc_error_print(stdout, ret);


  ret = CMEM_SegmentAllocate(1000, "CMEM_1", &handle);
  if (ret)
    rcc_error_print(stdout, ret);
  std::cout << "The handle is " << handle << std::endl;

  for (int i = 0; i < repeat_; i++) {

//    cout << " thread # " << serialNum_ << " event # " << i << endl;

// check that CMEM segments disappear on segfault
// if ( i > 1000000 && serialNum_ == 2) {
//   int* ill_ptr = 0x22334455;
//   *ill_ptr = 5;
//   ill_ptr++;
// }

  }

  sleep(20);		// send a KILL now to check that CMEM segments disappear

}

/*
  Cleanup procedure for the test thread
*/

void ThreadCmem::cleanup() {

  std::cout << " in cleanup() for thread " << serialNum_ << std::endl;

}

