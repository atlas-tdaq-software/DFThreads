#include <iostream>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

#include "ThreadSwitchYield.h"

#define NTHREADS 4
#define MAX_EVENTS 100000

using namespace std;

int s_threadId[NTHREADS*MAX_EVENTS];
int s_count = 0;

int main (int argc, char* argv[])
{

  int nth, noEvents;
  DFThread * thr[NTHREADS];
  int runTime=10;
  int hit4 = 0;

  if ((argc==2) && (isdigit(argv[1][0]))) {
     runTime=strtol(argv[1], 0, 10);
  }
  else if (argc != 1) {
     cerr << "Usage: " << argv[0]
          << " <ss>\nwhere <ss> is the time to run in seconds\n";
     exit (0);
  }

// fix # threads to 4 for simplicity

  nth = NTHREADS;

  std::cout << " # events = ( <" << MAX_EVENTS << ")  "; 
  cin >> noEvents;

//Create the test threads and start their execution
  for (int i = 0; i < nth; i++) {
    thr[i] = new ThreadSwitchYield(i,noEvents);
    thr[i]->startExecution();
  }

  //Give some time to the threads
  sleep(runTime);

  //When all the threads have sent their last message wait for them
  //to terminate cleanly
  for (int i = 0; i < nth; i++) {
    std::cout << "MAIN: waiting for termination of thread " << i << std::endl;
    thr[i]->stopExecution();
    try {
      thr[i]->waitForCondition(DFThread::TERMINATED, 100);
      std::cout << "MAIN: thread " << i << " terminated" << std::endl;
    }
    catch (DFThread::Timeout&) {
      std::cout << "MAIN: thread " << i << " did not terminate in time" << std::endl;
    }
  }

  std::cout << "MAIN: I am finished with the threads" << std::endl;

// just to get a feeling ..
  for (int i=0; i<100; i++) {
    std::cout << " threadID[" << i << "] = " << s_threadId[i] << std::endl;
  }

// find # quadruplets with different thread Ids
  int ip;
  for ( ip = 0; ip < noEvents*nth; ip++) {
    if (s_threadId[ip] == 0) {  // start with threadId = 0
#ifdef DEBUG
    std::cout << " s_threadId[0 to 3]: " << s_threadId[ip] << "  " << s_threadId[ip+1] << "  " <<
                                    s_threadId[ip+2] << "  " << s_threadId[ip+3] << std::endl;
#endif
      int it = 0;
      if ( it < nth && s_threadId[ip+1] != s_threadId[ip] ) {
        it++;
        if (it < nth &&
          s_threadId[ip+2] != s_threadId[ip+1] &&
          s_threadId[ip+2] != s_threadId[ip] ) {
          it++;
          if (it < nth &&
            s_threadId[ip+3] != s_threadId[ip+2] &&
            s_threadId[ip+3] != s_threadId[ip+1] &&
            s_threadId[ip+3] != s_threadId[ip] ) {
              hit4++;		// four different threads
          }
        }
      }
    }
  }

  std::cout << std::endl;
  std::cout << " # events = " << noEvents << std::endl;
  std::cout << " # quadruplets with different thread Ids = " << hit4 << std::endl;

  std::cout << std::endl;
  std::cout << " If # quadruplets is about the same as # events you may conclude that the" << std::endl;
  std::cout << " thread scheduling is round-robin (i.e. that the scheduler yield patch is active) " << std::endl;

}
