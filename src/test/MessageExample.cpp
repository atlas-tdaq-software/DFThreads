/*
    DFThread package
    Class: MessageExample
    Authors: B.Gorini, G.Lehmann
*/

#include "MessageExample.h"

#include <iostream>
#include <sstream>

/*
  Prints a message on deletion, to check the automatic
  garbage collection mechanism, provided by DFCountedPointer.
*/
MessageExample::~MessageExample(){
	std::cout << "MESSAGE: deleting " << * this << std::endl;
}

MessageExample::MessageExample(int threadId, int step, char * priority){
	std::ostringstream mStream;
  mStream << "\"THREAD " << threadId
	  << ": try number " << step 
	  << ": " << priority << " priority\"" 
	  << std::ends;
  append(mStream.str());
}
