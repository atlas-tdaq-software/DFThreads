/************************************************************************/
/*									*/
/*  file : ThreadSwitchYieldNoTstamp.cpp				*/
/*									*/
/* 021018  J.Petersen EP						*/
/*									*/
/* results: with libraries & test compiled with -O0			*/
/*									*/
/* each yield produces a context switch ( no fall through)		*/
/*									*/
/************************************************************************/

#include <iostream>

#include "ThreadSwitchYield.h"

// duplication ...
#define MAX_EVENTS 100000
#define NTHREADS 4

// globals
  extern int s_threadId[NTHREADS*MAX_EVENTS];
  extern int s_count;

/*
  Constructor of the test thread
*/

ThreadSwitchYield::ThreadSwitchYield(int serialNum, int repeat) : serialNum_(serialNum),
   repeat_(repeat) {
}

/*
  Body of the test thread (simply yield)
*/

void ThreadSwitchYield::run() {

  std::cout << "Started thread: " << DFThread::id() << std::endl ;

  for (int i = 0; i < repeat_; i++) {

//     cout << " thread # " << serialNum_ << " event # " << i << endl;

    s_threadId[s_count] = serialNum_;
    s_count++;

    DFThread::yield();
  }

}

/*
  Cleanup procedure for the test thread
*/

void ThreadSwitchYield::cleanup() {

  std::cout << " in cleanup() for thread " << serialNum_ << std::endl;

}

