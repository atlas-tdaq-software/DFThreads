/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include "DFThreads/DFIdentifyble.h"

std::string & DFIdentifyble::getName() { 
  return name_ ; 
}

void DFIdentifyble::setName(const char * name) { 
  name_ = name; 
}

