/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#include "DFThreads/DFFastMutex.h"

template class DFCreator < DFFastMutex >;

DFFastMutex::DFFastMutex() : locker_(0) {
  pthread_mutex_init(& (mutex_), 0);
}

DFFastMutex::~DFFastMutex() {
  pthread_mutex_destroy(& (mutex_));
}

DFFastMutex * DFFastMutex::Create(char * name) {
  DFFastMutex * obj = DFCreator < DFFastMutex >::Create(name);
  obj->setName(name);
  return obj;
}

DFFastMutex * DFFastMutex::Create() {
  return DFCreator < DFFastMutex >::Create();
}

int DFFastMutex::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFFastMutex >::Destroy(tmp.c_str());
  }
}


