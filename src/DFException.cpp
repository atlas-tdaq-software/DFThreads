/*
    DFThread package
    Class: DFException
    Authors: B.Gorini, G.Lehmann
*/

#include "DFThreads/DFException.h"

const char * DFException::what() const noexcept { 
  return m_text; 
}

DFException::DFException(const char * text) 
  : m_text(text) { }
