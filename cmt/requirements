package DFThreads

author  Benedetto.Gorini@cern.ch Giovanna.Lehmann@cern.ch
manager Andrei.Kazarov@cern.ch Benedetto.Gorini@cern.ch

#----------------------------------------------------------
use TDAQPolicy
use rcc_time_stamp
use cmem_rcc
#----------------------------------------------------------


#==========================================================
private
#==========================================================
macro c_opt_flags   " -O0 "
macro cpp_opt_flags " -O0 "

#macro_append cpp_opt_flags " -g -DTSTAMP "
#macro_remove cppflags "-fPIC" 

#----------------------------------------------------------
macro constituents ""                      \
      linux    "DFThreads testThreads testSwitchYield testSwitchYieldNoTstamp \
                testCmem testSwitchCondWait "
#----------------------------------------------------------
library     DFThreads   "DFIdentifyble.cpp \
                         DFException.cpp   \
                         DFLock.cpp   \
                         DFThread.cpp      \
                         DFMutex.cpp       \
                         DFFastMutex.cpp       \
                         DFSemaphore.cpp   \
                         DFConditional.cpp"
application testThreads "test/MessageExample.cpp \
                         test/ThreadExample.cpp  \
                         test/testThreads.cpp"
application testSwitchYield " test/ThreadSwitchYield.cpp  \
                         test/testSwitchYield.cpp"
application testSwitchYieldNoTstamp " test/ThreadSwitchYieldNoTstamp.cpp  \
                         test/testSwitchYieldNoTstamp.cpp"
application testCmem " test/ThreadCmem.cpp  \
                         test/testCmem.cpp"
application testSwitchCondWait " test/testSwitchCondWait.cpp"
application testSwitchTS " test/testSwitchTS.cpp"
application testSwitchMutexTS " test/testSwitchMutexTS.cpp"
#----------------------------------------------------------
macro testThreads_dependencies DFThreads
macro testSwitchYield_dependencies DFThreads
macro testSwitchYieldNoTstamp_dependencies DFThreads
macro testCmem_dependencies DFThreads
macro testSwitchCondWait_dependencies DFThreads
macro testSwitchTS_dependencies DFThreads
macro testSwitchMutexTS_dependencies DFThreads
#----------------------------------------------------------
macro app_testThreads_cppflags "-I../src/test"
macro app_testSwitchYield_cppflags "-I../src/test"
macro app_testSwitchYieldNoTstamp_cppflags "-I../src/test"
macro app_testCmem_cppflags "-I../src/test"
macro app_testSwitchCondWait_cppflags "-I../src/test"
macro app_testSwitchTS_cppflags "-I../src/test"
macro app_testSwitchMutexTS_cppflags "-I../src/test"
#----------------------------------------------------------
macro_append testThreadslinkopts " -lDFThreads "
macro_append testSwitchYieldlinkopts " -lDFThreads "
macro_append testSwitchYieldNoTstamplinkopts " -lDFThreads "
macro_append testCmemlinkopts " -lDFThreads -lcmem_rcc "
macro_append testSwitchCondWaitlinkopts " -lDFThreads "
#----- check target stuff -----
document script_launcher DFThreadscheck -group=check ../${DFThreads_tag}/testThreads
#----------------------------------------------------------


#==========================================================
public
#==========================================================

#----------------------------------------------------------
macro DFTHREADS_LIBRARIES ""  \
      linux "libDFThreads.so"
apply_pattern install_libs files=${DFTHREADS_LIBRARIES}
#----------------------------------------------------------
macro DFTHREADS_APPS ""  \
      linux "testThreads testSwitchYield testSwitchYieldNoTstamp testSwitchCondWait testCmem "
apply_pattern install_apps files=${DFTHREADS_APPS}
#----------------------------------------------------------
macro DFThreads_shlibflags " "
#macro DFThreads_linkopts "-lDFThreads"
#----------------------------------------------------------
