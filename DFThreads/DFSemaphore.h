/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFSEMAPHORE_H
#define DFSEMAPHORE_H

#include <pthread.h>

#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFException.h"

/**
 * Class providing semaphore synchronization, implemented using pthread
 * library and without using system calls nor signals.
 */
class DFSemaphore : public DFIdentifyble {
public:
  class Timeout : public DFException {
  public:
    Timeout() : DFException("This operation timed out") { }
  };

  static DFSemaphore * Create(char * name);
  static DFSemaphore * Create();
  void down();
  void down(long int timeout); //  throw(Timeout)
  int up();
  int decrement();
  int value();
  int destroy();
  virtual ~DFSemaphore();
  friend class DFCreator < DFSemaphore >;
private:
  pthread_mutex_t mutex_;
  pthread_cond_t cond_;
  int value_;
protected:
  DFSemaphore();
};

#endif //DFSEMAPHORE_H
