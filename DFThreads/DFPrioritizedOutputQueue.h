/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFPRIORITIZEDOUTPUTQUEUE_H
#define DFPRIORITIZEDOUTPUTQUEUE_H

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFOutputQueue.h"

/**
 * Generic output communication queue with a priority mechanism for the input.
 */

template < class T >
class DFPrioritizedOutputQueue : public DFOutputQueue < T > {
public:
  enum Priority { HIGH, NORMAL, LOW };

  /** Push an instance of class T to the queue. */
  virtual void push(T in) = 0;

  /**
   * Push an instance of class T to the queue with a specified priority.
   * Implementation of this method shall garantee that the elements are
   * inserted in the queue after all the elements with the same or a larger
   * priority and before all the elements with a lower priority (see
   * Priority).
   */
  virtual void push(T in, Priority priority) = 0;

  /**
   * Release the queue and delete it only if no other thread is
   * referencing it.
   */
  virtual int destroy() = 0;

  /** Returns the maximum number of elements that the queue can contain 
   *  or 0 if the queue is dynamically extensible.
   */
  virtual unsigned int size() = 0;

  /** Returns the number of elements contained in the queue. */
  virtual unsigned int numberOfElements() = 0;

  /**
   * Reset the queue. If there are still some elements inside 
   * it returns the pointer to a vector containing them so that 
   * the user can decide what to do with them. Otherwise it 
   * returns 0.
   */
  virtual std::vector<T> * reset(void) = 0 ;

};

#endif //DFPRIORITIZEDOUTPUTQUEUE_H

