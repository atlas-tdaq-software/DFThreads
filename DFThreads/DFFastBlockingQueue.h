/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFFASTBLOCKINGQUEUE_H
#define DFFASTBLOCKINGQUEUE_H

#include <pthread.h>
#include <cerrno>

#include "rcc_time_stamp/tstamp.h"

#include "DFThreads/DFCreator.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFInputQueue.h"
#include "DFThreads/DFOutputQueue.h"

/**
 * Blocking implementation of an input and output queue, based on an underlying
 * array. This queue has a fixed size (default = 100) which can be changed with
 * the method resize (BEWARE: calling resize on a non-empty queue will delete 
 * all the elements contained in the queue). 
 * Calling pop on an empty queue will block until one element is pushed in the 
 * queue. 
 * Calling push on a full queue will block until n elements (where n is specified
 * at queue contruction time by the "unblockingOffset" parameter) are popped out 
 * of the queue.
 * Class T has to provide a default and a copy constructor.
 * BEWARE: this queue cannot be used with reference counting pointers, because 
 * for performance reaseons, the elements are not explicitly deleted when pop() 
 * is called but only when/if they get overwritten!! 
 * If you need to hold reference counting pointers use DFStandardQueue instead.
 */

template < class T >
class DFFastBlockingQueue : public DFIdentifyble, public DFInputQueue < T >,
   public DFOutputQueue < T > {
   public:
     static const int DEFAULT_SIZE = 100 ; 
     virtual ~DFFastBlockingQueue();
     void push(T in);
     T swap(T in);
     T pop();
     T pop(long int timeout); // throw( typename DFInputQueue<T>::Timeout ) 
     bool empty();
     unsigned int size();
     unsigned int numberOfElements();
     int destroy();
     std::vector<T> * reset(void);
     typename DFInputQueue < T >::InStatistics getInStatistics();
     typename DFOutputQueue < T >::OutStatistics getOutStatistics();

     /**
      * Returns the instance of DFFastBlockingQueue<T> associated to name. If such
      * an instance already exists returns a pointer to it otherwise creates a
      * new instance, associates it to name an return the pointer to it. The
      * creation mechanism guarantees that all the threads calling
      * Create(name) with the same name will get a pointer to the same
      * instance, no matter the actual order of calls. The number of active
      * references to a specific instance are counted to guarantee that the
      * instance is deleted only when all the referring threads have called
      * the destroy() method on it.
      */
     static DFFastBlockingQueue < T > * Create(char * name);
     static DFFastBlockingQueue < T > * Create(char * name,
					     unsigned int size,
					     unsigned int unblockingOffset=1) ;

     /** Returns a new anonymous instance of DFFastBlockingQueue<T>. */
     static DFFastBlockingQueue < T > * Create();
     static DFFastBlockingQueue < T > * Create(unsigned int size,
					     unsigned int unblockingOffset=1) ;

     friend class DFCreator < DFFastBlockingQueue < T > >;
   private:
     pthread_mutex_t mutex_;
     pthread_cond_t cond_;
     unsigned int m_popIndex ;
     unsigned int m_pushIndex ;
     unsigned int m_elements ;
     unsigned int m_size ;
     unsigned int m_unblockingSize ; //Size at which pushes unblocks again
     unsigned int m_popQueueEmpty;
     unsigned int m_popThreshold;
     unsigned int m_pushQueueFull;
     unsigned int m_pushQueueEmpty;
     T *q;
     void size(unsigned int size,unsigned int unblockingOffset) ;
     static void cleanup_handler(void *arg) ;
   protected:
     DFFastBlockingQueue();
};

template < class T >
DFFastBlockingQueue < T > * DFFastBlockingQueue < T >::Create(char * name) {
  DFFastBlockingQueue < T > * obj =
     DFCreator < DFFastBlockingQueue < T > >::Create(name);
  obj->setName(name);
  return obj;
}

template < class T >
DFFastBlockingQueue < T > * DFFastBlockingQueue < T >::Create() {
  return DFCreator < DFFastBlockingQueue < T > >::Create();
}

template < class T >
DFFastBlockingQueue < T > * DFFastBlockingQueue < T >::Create(char * name,
						    unsigned int size,
						    unsigned int unblockingOffset) {
  DFFastBlockingQueue < T > * obj = DFCreator < DFFastBlockingQueue < T > >::Create(name);
  obj->setName(name);
  obj->size(size,unblockingOffset) ;
  return obj;
}

template < class T >
DFFastBlockingQueue < T > * DFFastBlockingQueue < T >::Create(unsigned int size,
						    unsigned int unblockingOffset) {
  DFFastBlockingQueue < T > * obj = Create() ; 
  obj->size(size,unblockingOffset) ;
  return obj;
}

template < class T >
int DFFastBlockingQueue < T >::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFFastBlockingQueue < T > >::Destroy(tmp.c_str());
  }
}

template < class T >
DFFastBlockingQueue < T >::~DFFastBlockingQueue() {
  pthread_mutex_destroy(& mutex_);
  pthread_cond_destroy(& cond_);
}

template < class T >
DFFastBlockingQueue < T >::DFFastBlockingQueue() 
  :  m_popIndex(0) ,
     m_pushIndex(0) ,
     m_elements(0),
     m_size(DEFAULT_SIZE),
     m_unblockingSize(m_size-1),
     m_popQueueEmpty(0),
     m_popThreshold(0),
     m_pushQueueFull(0),
     m_pushQueueEmpty(0),
     q(new T[m_size]) { 
  pthread_mutex_init(& (mutex_), 0);
  pthread_cond_init(& (cond_), 0);

}

template < class T >
void DFFastBlockingQueue < T >::size(unsigned int size,unsigned int unblockingOffset) {
  if (size != m_size) {
    if ( size > 1 ) {
      m_size = size ;
    }
    else {
      m_size = 1;
    }
    delete []q ;
    q = new T[m_size] ;
    m_popIndex = 0 ;
    m_pushIndex = 0 ;
    m_elements = 0 ;
  }
  if (unblockingOffset < m_size) {
    m_unblockingSize = m_size - unblockingOffset ;
  }
  else {
    m_unblockingSize = m_size - 1 ;
  }
}

template < class T >
T DFFastBlockingQueue < T >::pop() {

 TS_RECORD(TS_H1,8000);

  pthread_mutex_lock(& mutex_);
 TS_RECORD(TS_H1,8001);
  while (m_elements==0) {
 TS_RECORD(TS_H1,8002);
    m_popQueueEmpty++;
    pthread_cleanup_push(cleanup_handler,(void *)&mutex_);
    pthread_cond_wait(& cond_, & mutex_);
    pthread_cleanup_pop(0);
 TS_RECORD(TS_H1,8003);
  }
 TS_RECORD(TS_H1,8004);
  unsigned int newElements = --m_elements;  
  T element = q[m_popIndex];
  m_popIndex++; m_popIndex=m_popIndex%m_size ;
  pthread_mutex_unlock(& mutex_);
 TS_RECORD(TS_H1,8005);
  if (newElements==m_unblockingSize) {
    m_popThreshold++;
    pthread_cond_broadcast(& cond_);
  }

 TS_RECORD(TS_H1,8009);

  return element;
}

template < class T >
T DFFastBlockingQueue < T >::pop(long int timeout) /* throw( typename DFInputQueue<T>::Timeout ) */ {
  pthread_mutex_lock(& mutex_);
  if (m_elements==0) {
    struct timespec timeLimit = { time(0)+timeout , 0 };
    while (m_elements==0) {    
      pthread_cleanup_push(cleanup_handler,(void *)&mutex_);
      if (pthread_cond_timedwait(& cond_, & mutex_, & timeLimit) == ETIMEDOUT) {
	pthread_mutex_unlock(& mutex_);
	throw DFInputQueue<T>::Timeout() ;
      }
      pthread_cleanup_pop(0);
    }
  }
  unsigned int newElements = --m_elements;
  T element = q[m_popIndex];
  m_popIndex++; m_popIndex=m_popIndex%m_size ;
  pthread_mutex_unlock(& mutex_);
  if (newElements==m_unblockingSize) pthread_cond_broadcast(& cond_);
  return element ;
}

template < class T >
T DFFastBlockingQueue < T >::swap(T in) {

  T element ; 
 TS_RECORD(TS_H1,8500);

  pthread_mutex_lock(& mutex_);
 TS_RECORD(TS_H1,8501);
  if (m_elements>0) {
 TS_RECORD(TS_H1,8502);
    element = q[m_popIndex];
    m_popIndex++; m_popIndex=m_popIndex%m_size ;
    q[m_pushIndex] = in ;
    m_pushIndex++; m_pushIndex=m_pushIndex%m_size ;
 TS_RECORD(TS_H1,8503);
  }
  else {
    element = in;
  }  
  pthread_mutex_unlock(& mutex_);

 TS_RECORD(TS_H1,8504);

  return element;
}

template < class T >
void DFFastBlockingQueue < T >::push(T in) {

 TS_RECORD(TS_H1,8110);

  pthread_mutex_lock(& mutex_);
 TS_RECORD(TS_H1,8111);
  while (m_elements==m_size) {
    m_pushQueueFull++;
    pthread_cleanup_push(cleanup_handler,(void *)&mutex_);
    pthread_cond_wait(& cond_, & mutex_);
    pthread_cleanup_pop(0);
  }
 TS_RECORD(TS_H1,8112);
  q[m_pushIndex] = in ;
  m_pushIndex++; m_pushIndex=m_pushIndex%m_size ;
  unsigned int oldElements = m_elements++;
  pthread_mutex_unlock(& mutex_);
  if (oldElements==0) {
 TS_RECORD(TS_H1,8113);
    m_pushQueueEmpty++;
    pthread_cond_broadcast(& cond_);
  }

 TS_RECORD(TS_H1,8119);
}

template < class T >
bool DFFastBlockingQueue < T >::empty() {
  return (m_elements == 0);
}

template < class T >
unsigned int DFFastBlockingQueue < T >::size() {
  return m_size;
}

template < class T >
unsigned int DFFastBlockingQueue < T >::numberOfElements() {
  return m_elements;
}

template < class T >
typename DFInputQueue < T >::InStatistics DFFastBlockingQueue < T >::getInStatistics() {
  DFInputQueue < T >::m_statistics.popQueueEmptySum = m_popQueueEmpty;
  DFInputQueue < T >::m_statistics.popThresholdSum = m_popThreshold;
  return (DFInputQueue < T >::m_statistics);
}

template < class T >
typename DFOutputQueue < T >::OutStatistics DFFastBlockingQueue < T >::getOutStatistics() {
  DFOutputQueue < T >::m_statistics.pushQueueEmptySum = m_pushQueueEmpty;
  DFOutputQueue < T >::m_statistics.pushQueueFullSum = m_pushQueueFull;
  return (DFOutputQueue < T >::m_statistics);
}

template < class T >
void DFFastBlockingQueue < T >::cleanup_handler(void *arg) {
  pthread_mutex_unlock((pthread_mutex_t *)arg);
}

template < class T >
std::vector<T> * DFFastBlockingQueue < T >::reset() {
  std::vector<T> * rc = 0;

  pthread_mutex_lock(& mutex_);

  if (m_elements!=0) {
    rc = new std::vector<T>;
    rc->reserve(m_elements);
    while (m_elements>0) {
      rc->push_back(q[m_popIndex]);
      m_popIndex++; m_popIndex=m_popIndex%m_size ;
      m_elements--;
    }
  }
  m_popIndex = 0;
  m_pushIndex = 0;
  m_elements = 0;
  m_popQueueEmpty = 0;
  m_popThreshold = 0;
  m_pushQueueFull = 0;
  m_pushQueueEmpty = 0;

  pthread_cond_broadcast(& cond_);	// unlock waiting threads

  pthread_mutex_unlock(& mutex_);
  return rc;
}

#endif //DFFASTBLOCKINGQUEUE_H
