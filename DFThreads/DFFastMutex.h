/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: Wrapper for the pthread mutex
            library
   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFFASTMUTEX_H
#define DFFASTMUTEX_H

#include <pthread.h>

#include "DFThreads/DFCreator.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFException.h"

/**
 * Provides the mutex (MUTual EXclusion) synchronization mechanism. The lock
 * on a DFFastMutex instance is granted to only one thread at a time. Threads
 * requesting to lock a DFFastMutex which is already locked by another thread are
 * blocked with no CPU consumption until the lock is released. DFFastMutex
 * implementation is based on the standard pthread mutex with no additional checks.
 * For a safer but slower implementation see DFMutex class.
 */
class DFFastMutex : public DFIdentifyble {
public:
  /**
   * Returns the instance of DFFastMutex associated to name. If such an instance
   * already exists returns a pointer to it otherwise creates a new instance,
   * associates it to name an returns the pointer to it. The creation
   * mechanism guarantees that all the threads calling Create(name) with the
   * same name will get a pointer to the same instance, no matter the actual
   * order of calls. The number of active references to a specific instance is
   * counted to guarantee that the instance is deleted only when all the
   * referring threads have called the destroy() method on it.
   */
  static DFFastMutex * Create(char * Name);

  /** Creates a new anonymous instance of DFFastMutex. */
  static DFFastMutex * Create();

  /**
   * Release the reference to the DFFastMutex instance and delete it only if no
   * other thread is referencing it.
   */
  int destroy();

  /**
   * Releases the lock on the DFFastMutex.  
   */
  void unlock() ;

  /**
   * Requests to obtain the lock on the DFFastMutex. The method blocks the thread
   * execution with no CPU consumption as long as the DFFastMutex is locked by
   * another thread. 
   */
  void lock() ;

  /**
   * Requests to obtain the lock on the DFFastMutex. The method return
   * immediately. The return value specifies if the mutex was actually
   * locked or not.
   */
  bool trylock() ;

  virtual ~DFFastMutex();
  friend class DFCreator < DFFastMutex >;
private:
  pthread_t locker_;
  pthread_mutex_t mutex_;
protected:
  DFFastMutex();
};

inline void DFFastMutex::lock() {
  pthread_mutex_lock(& mutex_);
}

inline bool DFFastMutex::trylock() {
  bool retval=false;
  if (pthread_mutex_trylock(& mutex_)==0) retval=true;
  return retval;
}

inline void DFFastMutex::unlock() {
  pthread_mutex_unlock(& mutex_);
}

#endif //DFFASTMUTEX_H

