/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFSTANDARDPRIORITIZEDQUEUE_H
#define DFSTANDARDPRIORITIZEDQUEUE_H

#include <queue>
#include "DFThreads/DFMutex.h"
#include "DFThreads/DFSemaphore.h"
#include "DFThreads/DFCreator.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFInputQueue.h"
#include "DFThreads/DFPrioritizedOutputQueue.h"

/**
 * Basic implementation of an input queue and a prioritized output queue,
 * based on 3 underlying STL queues. Required memory is allocated/deallocated
 * dynamically. Class T has to provide a default and a copy constructor.
 */

template < class T >
class DFStandardPrioritizedQueue : public DFIdentifyble,
   public DFInputQueue < T >, public DFPrioritizedOutputQueue < T > {
   public:
     virtual ~DFStandardPrioritizedQueue();
     void push(T in);
     void push(T in, const typename DFPrioritizedOutputQueue < T >::Priority priority);
     T swap(T in);
     T pop();
     T pop(long int timeout); //  throw( typename DFInputQueue<T>::Timeout )
     bool empty();
     unsigned int size();
     unsigned int numberOfElements();
     int destroy();
     std::vector<T> * reset(void);
     typename DFInputQueue <T>::InStatistics getInStatistics();
     typename DFOutputQueue <T>::OutStatistics getOutStatistics();

     /**
      * Returns the instance of DFStandardPrioritizedQueue<T> associated to
      * name. If such an instance already exists returns a pointer to it
      * otherwise creates a new instance, associates it to name an return the
      * pointer to it. The creation mechanism guarantees that all the threads
      * calling Create(name) with the same name will get a pointer to the same
      * instance, no matter the actual order of calls. The number of active
      * references to a specific instance are counted to guarantee that the
      * instance is deleted only when all the referring threads have called
      * the destroy() method on it.
      */
     static DFStandardPrioritizedQueue < T > * Create(char * name);

     /** Returns a new anonymous instance of DFStandardPrioritizedQueue<T>. */
     static DFStandardPrioritizedQueue < T > * Create();
     friend class DFCreator < DFStandardPrioritizedQueue < T > >;
   private:
    enum Priority { HIGH, NORMAL, LOW };
     DFMutex * mutex_;
     DFSemaphore * semaphore_;
     std::queue < T > q[LOW + 1];
   protected:
     DFStandardPrioritizedQueue();
};

template < class T >
DFStandardPrioritizedQueue < T >::~DFStandardPrioritizedQueue() {
  mutex_->destroy();
  semaphore_->destroy();
}

template < class T >
DFStandardPrioritizedQueue < T >::DFStandardPrioritizedQueue() {
  mutex_ = DFMutex::Create();
  semaphore_ = DFSemaphore::Create();
}

template < class T >
DFStandardPrioritizedQueue < T > * DFStandardPrioritizedQueue < T >
   ::Create(char * name) {
     DFStandardPrioritizedQueue < T > * obj =
        DFCreator < DFStandardPrioritizedQueue < T > >::Create(name);
     obj->setName(name);
     return obj;
}

template < class T >
DFStandardPrioritizedQueue < T > * DFStandardPrioritizedQueue < T >
   ::Create() {
     return DFCreator < DFStandardPrioritizedQueue < T > >::Create();
}

template < class T >
int DFStandardPrioritizedQueue < T >::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFStandardPrioritizedQueue < T > >
       ::Destroy(tmp.c_str());
  }
}

template < class T >
void DFStandardPrioritizedQueue < T >::push(T in) {
  push(in, NORMAL);
}

template < class T >
void DFStandardPrioritizedQueue < T >
   ::push(T in, const typename DFPrioritizedOutputQueue < T >::Priority priority) {
     mutex_->lock();
     q[priority].push(in);
     semaphore_->up();
     mutex_->unlock();
}

template < class T >
T DFStandardPrioritizedQueue < T >::swap(T in) {
  mutex_->lock();
  T element = in;
  if (!empty()) {
    int i;
    for (i = HIGH; q[i].empty() && i < LOW; i++) ;
    element = q[i].front();
    q[i].pop();
    q[LOW].push(in);
  }
  mutex_->unlock();
  return element;
}

template < class T >
T DFStandardPrioritizedQueue < T >::pop() {
  semaphore_->down();
  mutex_->lock();
  int i;
  for (i = HIGH; q[i].empty() && i < LOW; i++) ;
  T element = q[i].front();
  q[i].pop();
  mutex_->unlock();
  return element;
}

template < class T >
T DFStandardPrioritizedQueue < T >::pop(long int timeout) {
     /* throw( typename DFInputQueue<T>::Timeout ) { */
  try {
    semaphore_->down(timeout) ;
    mutex_->lock();
    int i;
    for (i = HIGH; q[i].empty() && i < LOW; i++) ;
    T element = q[i].front();
    q[i].pop();
    mutex_->unlock();
    return element;
  }
  catch (DFSemaphore::Timeout&) {
    throw DFInputQueue<T>::Timeout() ;
  }
}

template < class T >
bool DFStandardPrioritizedQueue < T >::empty() {
  return (semaphore_->value() > 0);
}

template < class T >
unsigned int DFStandardPrioritizedQueue < T >::size() {
  return 0;
}

template < class T >
unsigned int DFStandardPrioritizedQueue < T >::numberOfElements() {
  return (unsigned int)(semaphore_->value());
}

template < class T >
typename DFInputQueue <T>::InStatistics DFStandardPrioritizedQueue < T >::getInStatistics() {
  return (DFInputQueue <T>::m_statistics);		// FIXME: to be done
}

template < class T >
typename DFOutputQueue <T>::OutStatistics DFStandardPrioritizedQueue < T >::getOutStatistics() {
  return (DFOutputQueue <T>::m_statistics);		// FIXME: to be done
}

template < class T >
std::vector<T> * DFStandardPrioritizedQueue < T >::reset() {
  //To Be Implemented
  return 0;
}

#endif //DFSTANDARDPRIORITIZEDQUEUE_H
