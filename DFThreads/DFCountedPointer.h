/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: DFCountedPointer is a template
            class for intelligent pointers
   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFCOUNTEDPOINTER_H
#define DFCOUNTEDPOINTER_H

#include <pthread.h>
#include "DFThreads/DFLock.h"

/**
 * Thread-safe intelligent pointer for semi-automatic garbage collection.
 * Instances of DFCountedPointer<T> provide access to the members of class T
 * through the -> operator. If instances of DFCountedPointer<T> are
 * consistently used instead of normal pointers to T they will keep track of
 * the number of active references to the instances of T and automatically
 * delete them when no longer needed. Instances of class DFCountedPointer<T>
 * shall neither be dereferenced nor passed to methods by reference! This will
 * not induce any performance penality as a DFCountedPointer<T> instance
 * allocates the same memory space as a normal pointer.
 */

template < class T >
class DFCountedPointer {
public:

  /** Default constructor. Instances of DFCountedPointer<T> created with this
   * constructor do not point to any instance of T (think at them as the
   * analogous of null pointers) and isNotNull() method will return
   * false for them. */
  DFCountedPointer() ;

  /**
   * Creates an instance of DFCountedPointer<T> pointing to an instance of T.
   * The istance of T SHALL be created anonymously ("DFCountedPointer<T> p =
   * new DFCountedPointer<T>(new T)") to ensure that no reference to it is
   * hold by normal pointer variables, and thus that its allocated memory will
   * be freed automatically.
   */
  DFCountedPointer(T * const pointer) ;

  //Destructor
  ~DFCountedPointer() ;

  //Copy constructor
  DFCountedPointer(const DFCountedPointer & cp) ;

  //Assignment operators
  DFCountedPointer & operator=(const DFCountedPointer & cp) ;

  //Dereference operators
  T * operator->() const ;

  /**
   * Check if this DFCountedPointer<T> is not pointing to any instance of T.
   * Equivalent to the check for a simple pointer for being equal to
   * 0 or NULL.
   */
  bool isNotNull() ;

  //Comparison operators
  bool operator == (DFCountedPointer < T > otherPointer) ;
  bool operator != (DFCountedPointer < T > otherPointer) ;

private:

  class DFGenericReference {
    private:
    int nReferences_;
    pthread_mutex_t mutex_;
    public:
    T * const pointer_;
    DFGenericReference(T * const pointer) ; 
    ~DFGenericReference() ;
    int increaseReferences() ;
    int decreaseReferences() ;
  };

  DFGenericReference * reference_ ;
};


template <class T> 
inline DFCountedPointer<T>::DFCountedPointer() : reference_(0) { }

template <class T> 
inline DFCountedPointer<T>::DFCountedPointer(T * const pointer) {
  if (pointer == 0) {
    reference_ = 0;
  }
  else {
    reference_ = new DFGenericReference (pointer);
  }
}

template <class T> 
inline DFCountedPointer<T>::~DFCountedPointer() {
  if (reference_ != 0 && reference_->decreaseReferences() == 0) {
    delete reference_;
  }
}

template <class T> 
inline DFCountedPointer<T>::DFCountedPointer(const DFCountedPointer & cp) {
  reference_ = cp.reference_;
  if (reference_ != 0) reference_->increaseReferences();
}

template <class T> 
inline DFCountedPointer<T> & DFCountedPointer<T>::operator=(const DFCountedPointer & cp) {
  if (reference_ != 0 && reference_->decreaseReferences() == 0) {
    delete reference_;
  }
  reference_ = cp.reference_;
  if (reference_ != 0) reference_->increaseReferences();
  
  return * this;
}

template <class T> 
inline T * DFCountedPointer<T>::operator->() const { return reference_->pointer_; }

template <class T> 
inline bool DFCountedPointer<T>::isNotNull() {
  return (reference_ != 0);
}

template <class T> 
inline bool DFCountedPointer<T>::operator==(DFCountedPointer < T > otherPointer) {
  return (reference_ == otherPointer.reference_);
}

template <class T> 
inline bool DFCountedPointer<T>::operator!=(DFCountedPointer < T > otherPointer) {
  return !((* this) == otherPointer);
}

template < class T >
inline DFCountedPointer<T>::DFGenericReference::DFGenericReference(T * const pointer) 
  : nReferences_(1), pointer_(pointer) {
  pthread_mutex_init(& mutex_, 0);
}

template < class T >
inline DFCountedPointer<T>::DFGenericReference::~DFGenericReference() {
  delete pointer_;
  pthread_mutex_destroy(& mutex_);
}
    
template < class T >
inline int DFCountedPointer<T>::DFGenericReference::increaseReferences() {
  DFLock lock(& mutex_);
  nReferences_++;
  return nReferences_;
}
    
template < class T >
inline int DFCountedPointer<T>::DFGenericReference::decreaseReferences() {
  DFLock lock(& mutex_);
  nReferences_--;
  return nReferences_;
}


#endif //DFCOUNTEDPOINTER_H

