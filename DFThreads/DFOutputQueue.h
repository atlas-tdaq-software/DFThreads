/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFOUTPUTQUEUE_H
#define DFOUTPUTQUEUE_H

#include <vector>

/** Generic output communication queue. */

template < class T >
class DFOutputQueue {
public:

  /** Push an instance of class T to the queue. */
  virtual void push(T in) = 0;

  /**
   * Release the queue and delete it only if no other thread is
   * referencing it.
   */
  virtual int destroy() = 0;

  /** Returns the maximum number of elements that the queue can contain 
   *  or 0 if the queue is dynamically extensible.
   */
  virtual unsigned int size() = 0;

  /** Returns the number of elements contained in the queue. */
  virtual unsigned int numberOfElements() = 0;

  /**
   * Reset the queue. If there are still some elements inside 
   * it returns the pointer to a vector containing them so that 
   * the user can decide what to do with them. Otherwise it 
   * returns 0.
   */
  virtual std::vector<T> * reset(void) = 0 ;

  protected:

    struct OutStatistics {
        int pushQueueFullSum;
        int pushQueueEmptySum;
    };

    OutStatistics m_statistics;

  public:

  virtual OutStatistics getOutStatistics() = 0;

};

#endif //DFOUTPUTQUEUE_H

