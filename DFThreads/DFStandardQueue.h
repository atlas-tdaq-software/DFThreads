/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFSTANDARDQUEUE_H
#define DFSTANDARDQUEUE_H

#include <queue>
#include <vector>
#include "DFThreads/DFFastMutex.h"
#include "DFThreads/DFSemaphore.h"
#include "DFThreads/DFCreator.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFInputQueue.h"
#include "DFThreads/DFOutputQueue.h"

/**
 * Basic implementation of an input and output queues, based on an underlying
 * STL queue. Required memory is allocated/deallocated dynamically. Class T
 * has to provide a default and a copy constructor.
 */

template < class T >
class DFStandardQueue : public DFIdentifyble, public DFInputQueue < T >,
   public DFOutputQueue < T > {
   public:
     virtual ~DFStandardQueue();
     void push(T in);
     T swap(T in);
     T pop();
     T pop(long int timeout); // throw( typename DFInputQueue<T>::Timeout )
     bool empty();
     unsigned int size();
     unsigned int numberOfElements();
     int destroy();
     std::vector<T> * reset(void);
     typename DFInputQueue <T>::InStatistics getInStatistics();
     typename DFOutputQueue <T>::OutStatistics getOutStatistics();

     /**
      * Returns the instance of DFStandardQueue<T> associated to name. If such
      * an instance already exists returns a pointer to it otherwise creates a
      * new instance, associates it to name an return the pointer to it. The
      * creation mechanism guarantees that all the threads calling
      * Create(name) with the same name will get a pointer to the same
      * instance, no matter the actual order of calls. The number of active
      * references to a specific instance are counted to guarantee that the
      * instance is deleted only when all the referring threads have called
      * the destroy() method on it.
      */
     static DFStandardQueue < T > * Create(char * name);

     /** Returns a new anonymous instance of DFStandardQueue<T>. */
     static DFStandardQueue < T > * Create();
     friend class DFCreator < DFStandardQueue < T > >;
   private:
     DFFastMutex * mutex_;
     DFSemaphore * semaphore_;
     std::queue < T > q;
   protected:
     DFStandardQueue();
};

template < class T >
DFStandardQueue < T > * DFStandardQueue < T >::Create(char * name) {
  DFStandardQueue < T > * obj =
     DFCreator < DFStandardQueue < T > >::Create(name);
  obj->setName(name);
  return obj;
}

template < class T >
DFStandardQueue < T > * DFStandardQueue < T >::Create() {
  return DFCreator < DFStandardQueue < T > >::Create();
}

template < class T >
int DFStandardQueue < T >::destroy() {
  if (getName().empty()) {
    delete this;
    return 0;
  }
  else {
    auto tmp = getName();
    return DFCreator < DFStandardQueue < T > >::Destroy(tmp.c_str());
  }
}

template < class T >
DFStandardQueue < T >::~DFStandardQueue() {
  mutex_->destroy();
  semaphore_->destroy();
}

template < class T >
DFStandardQueue < T >::DFStandardQueue() {
  mutex_ = DFFastMutex::Create();
  semaphore_ = DFSemaphore::Create();
}

template < class T >
T DFStandardQueue < T >::pop() {
  semaphore_->down();
  mutex_->lock();
  T element = q.front();
  q.pop();
  mutex_->unlock();
  return element;
}

template < class T >
T DFStandardQueue < T >::pop(long int timeout) /* throw( typename DFInputQueue<T>::Timeout ) */ {
  try {
    semaphore_->down(timeout);
    mutex_->lock();
    T element = q.front();
    q.pop();
    mutex_->unlock();
    return element ;
  }
  catch (DFSemaphore::Timeout&) {
    throw typename DFInputQueue<T>::Timeout() ;
  }
}

template < class T >
T DFStandardQueue < T >::swap(T in) {
  mutex_->lock();
  T element = in;
  if (!empty()) {
    element = q.front();
    q.pop();
    q.push(in);
  }  
  mutex_->unlock();
  return element;
}

template < class T >
void DFStandardQueue < T >::push(T in) {
  mutex_->lock();
  q.push(in);
  semaphore_->up();
  mutex_->unlock();
}

template < class T >
bool DFStandardQueue < T >::empty() {
  return (semaphore_->value() == 0);
}

template < class T > 
unsigned int  DFStandardQueue < T >::size() {
  return 0;
}

template < class T > 
unsigned int  DFStandardQueue < T >::numberOfElements() {
  return (unsigned int)(semaphore_->value());
}

template < class T >
typename DFInputQueue <T>::InStatistics DFStandardQueue < T >::getInStatistics() {
  return (DFInputQueue <T>::m_statistics);              // FIXME: to be done
}

template < class T >
typename DFOutputQueue <T>::OutStatistics DFStandardQueue < T >::getOutStatistics() {
  return (DFOutputQueue <T>::m_statistics);              // FIXME: to be done
}

template < class T >
std::vector<T> * DFStandardQueue < T >::reset() {

  std::vector<T>* queueContent = new std::vector<T>;
  mutex_->lock();
  queueContent->reserve(size());
  while(!q.empty()) {
    semaphore_->down();
    queueContent->push_back(q.front());
    q.pop();
  }
  mutex_->unlock();

  return queueContent;
}
#endif //DFSTANDARDQUEUE_H
