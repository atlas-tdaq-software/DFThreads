/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFINPUTQUEUE_H
#define DFINPUTQUEUE_H
#include "DFThreads/DFException.h"

/** Input communication queue interface. */

template < class T >
class DFInputQueue {
public:

  class Timeout : public DFException {
  public:
    Timeout() : DFException("This operation timed out") { }
  };

  /**
   * Get next available instance of class T from the input queue. Concrete
   * implementations of this method shall not return as long as the queue is
   * empty. The thread calling pop() method on an empty queue shall be blocked
   * with no CPU consumption.
   */
  virtual T pop() = 0;

  /**
   * Get next available instance of class T from the input queue. Concreteimplementations of this method shall not return as long as the queue is empty.
   * If the number of seconds specified by timeout parameter are passed the implementations shall throw Timeout exception.
   * The thread calling pop() method on an empty queue shall be blocked with no CPU consumption.
   */
  virtual T pop(long int timeout) = 0; /* throw( Timeout ) */ 

  /**
   * Get next available instance of class T from the input queue and symultaneously queue 
   * the specified T instance in. 
   * Concrete implementations of this method shall never block and return the input argument in
   * if the queue is empty. 
   */
  virtual T swap(T in) = 0;

  /** Test if the queue is empty. */
  virtual bool empty() = 0;

  /** Returns the maximum number of elements that the queue can contain 
   *  or 0 if the queue is dynamically extensible.
   */
  virtual unsigned int size() = 0;

  /** Returns the number of elements contained in the queue. */
  virtual unsigned int numberOfElements() = 0;

  /**
   * Release the queue and delete it only if no other thread is
   * referencing it.
   */
  virtual int destroy() = 0;

  protected:

    struct InStatistics {
        int popQueueEmptySum;
        int popThresholdSum;
    };

    InStatistics m_statistics;

  public:

  virtual InStatistics getInStatistics() = 0;

};

#endif //DFINPUTQUEUE_H

