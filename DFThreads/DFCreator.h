/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: DFCreator provides the means to
            share the same instance of an
	    object across threads
   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFCREATOR_H
#define DFCREATOR_H

#include <map>

#include <pthread.h>
#include <string>
#include "DFThreads/DFLock.h"

/*
struct eqstr {
  bool operator() (const char * s1, const char * s2) const {
    cout << "s1 = " << s1 << ", s2 = " << s2 << endl;
    bool result = (strcmp(s1, s2) == 0);
    cout << "eqstr result = " << result << endl;
    return result;
  }
};
*/

template < class T >
class DFIndexedObject : public T {
private:
  int instancesIndex_;

public:
  DFIndexedObject() {
    instancesIndex_ = 1;
  }

  int getIndex() {
    return instancesIndex_;
  }

  void incrementIndex() {
    instancesIndex_++;
  }

  void decrementIndex() {
    instancesIndex_--;
  }
};

/**
 * Provides the functionality for creating/deleting named instances of class T
 * that are shared between different threads. All threads independently
 * calling  DFCreator<T>::Create(name) with the same name prameter, shall
 * obtain a pointer to the same instance of class T. When a thread does not
 * need a named instance of T anymore it shall call
 * DFCreator<T>::Destroy(name) instead of deleting it. DFCreator<T>
 * synchronize the creation/destruction of the named instances, ensuring that
 * the actual instantiation happens only when the first thread calls
 * Create(name) and that the actual deletion happens only when the last thread
 * calls Destroy(name). IMPORTANT: pointers created with the DFCreator<T>
 * shall not be passed to other threads: the deletion mechanism will only work
 * if all threads consistently get the named instances of T through the
 * Create(name) method. The present DFCreator implementation works only for
 * classes T which are instantiated by the default (i.e. with no parameters)
 * constructor.
 */

template < class T >
class DFCreator {
public:

  /** Return the pointer to a named instance of class T. */
  static T * Create(char * name);

  /**
   * Create an anonymous instance of T. Anonymous instances are not shareable
   * between different threads. This method is provided for consistency.
   */
  static T * Create();

  /**
   * Decrease the number of threads referencing this named instance of T and
   * delete it when no thread is using it anymore.
   * Returns the number of remaining references to this object.
   */
  static int Destroy(const char * name);
private:
  static std::map <std::string, DFIndexedObject < T > *> namedInstances_;
  static pthread_mutex_t * MutexInit();
  static pthread_mutex_t * mutex_;
};

template < class T >
std::map <std::string, DFIndexedObject < T > *> DFCreator < T >::namedInstances_;

template < class T >
pthread_mutex_t * DFCreator < T >::MutexInit() {
  static pthread_mutex_t m;
  pthread_mutex_init(& (m), 0);
  return & m;
}

template < class T >
pthread_mutex_t * DFCreator < T >::mutex_ = DFCreator < T >::MutexInit();

template < class T >
T * DFCreator < T >::Create(char * name) {

  DFLock lock(mutex_);
  if (!namedInstances_[name]) {
    namedInstances_[name] = new DFIndexedObject < T >;
  }
  else {
    namedInstances_[name]->incrementIndex();
  }

  return namedInstances_[name];
}

template < class T >
T * DFCreator < T >::Create() {
  return new T;
}

template < class T >
int DFCreator < T >::Destroy(const char * name) {
  DFLock lock(mutex_);
  int rc = -1;

  if (name != 0 && namedInstances_[name]->getIndex()) {
    namedInstances_[name]->decrementIndex();
    if (namedInstances_[name]->getIndex() == 0) {
      delete namedInstances_[name];
      namedInstances_.erase(name);
      rc = 0;
    }
    else {
      rc = namedInstances_[name]->getIndex();
    }
  }

  return rc;
}

#endif //DFCREATOR_H
