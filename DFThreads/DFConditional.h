/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: DFConditional allows to wait and
            change the value of a condition
	    variable.

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFCONDITIONAL_H
#define DFCONDITIONAL_H

#include "DFThreads/DFException.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFMutex.h"

/**
 * Class providing conditional synchronisation mechanism. Threads use
 * instances of DFConditional to wait for a change of some condition or to
 * signal the occurrence of that change to one or all the threads waiting for
 * it. Threads waiting for a signal are blocked with no CPU consumption.
 */
class DFConditional : public DFIdentifyble {
public:

  class AlreadyLockedByThread : public DFException {
  public:
    AlreadyLockedByThread() :
       DFException("The internal mutex is already locked by this thread") { }
  };

  class NotLockedByThread : public DFException {
  public:
    NotLockedByThread() : DFException("The internal mutex is unlocked or is locked by another thread") {
    }
  };

  class Timeout : public DFException {
  public:
    Timeout() : DFException("An operation timed out") { }
  };

  /** Creates a new anonymous instance of DFConditional. */
  static DFConditional * Create();

  /**
   * Returns the instance of DFConditional associated to name. If such an
   * instance already exists returns a pointer to it otherwise creates a new
   * instance, associates it to name an returns the pointer to it. The
   * creation mechanism guarantees that all the threads calling Create(name)
   * with the same name will get a pointer to the same instance, no matter the
   * actual order of calls. The number of active references to a specific
   * instance is counted to guarantee that the instance is deleted only when
   * all the referring threads have called the destroy() method on it.
   */
  static DFConditional * Create(char * name);

  /**
   * Release the reference to the DFConditional instance and delete it only if
   * no other thread is referencing it.
   */
  int destroy();

  /**
   * Signals a change of condition to the thread possibly waiting for it. If
   * more than a thread is waiting for the same condition the signal goes to
   * the one  determined on the basis of thread scheduling priority and (if
   * the priority is equivalent) fist-in-first-out ordering.
   */
  void signal();

  /** Signals a change of condition to all the threads waiting for it. */
  void broadcast();

  /**
   * Requests to obtain the lock on the DFConditional internal mutex. If the
   * same thread then calls the wait() method the lock is temporarily released
   * until a change of condition is not signaled. Throws AlreadyLocked if
   * called by a thread which is already locking the mutex, and do not perform
   * the lock request which would otherwise result in a deadlock situation.
   */
  void lock(); // throw(AlreadyLockedByThread) 

  /**
   * Releases the lock on the DFConditional internal mutex. Throws
   * AlradyUnlocked if the mutex is unlocked. Denies the unlock request and
   * returns LockedByOther if the calling thread is not the one which owns the
   * lock on the mutex.
   */
  void unlock(); // throw(NotLockedByThread) 

  /**
   * Wait for a change of condition to be signaled by another thread. If the
   * thread calling wait has previously locked the DFConditional internal
   * mutex (calling the lock() method) this lock is temporarily released until
   * the change of condition is signaled, and is granted again on reception of
   * the change signal. The wait() method is a thread cancellation point.
   */
  void wait();

  /**
   * Wait for a change of condition to be signaled by another thread. If the
   * thread calling wait has previously locked the DFConditional internal
   * mutex (calling the lock() method) this lock is temporarily released until
   * the change of condition is signaled, and is granted again on reception of
   * the change signal. The wait() method is a thread cancellation point.
   * Throws Timeout if no change of condition signal has been received for the
   * specified number of seconds.
   */
  void wait(long int timeout);  // throw(Timeout) 
  friend class DFCreator < DFConditional >;
private:
  pthread_t locker_;
  pthread_t old_locker_;
  pthread_mutex_t mutex_;
  pthread_cond_t cond_;
protected:
  virtual ~DFConditional();
  DFConditional();
};

#endif //DFCONDITIONAL_H

