/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: locking-unlocking mutexes by 
            creation/destruction of the class
   Authors: G. Lehmann, B. Gorini, R. Hauser
   Version 1.0 - 12 December 2007
----------------------------------------*/

#ifndef DFLOCK_H
#define DFLOCK_H

#include <pthread.h>

class DFMutex;

class DFLock {
public:
  DFLock(pthread_mutex_t * mutex);
  DFLock(DFMutex * mutex);
  ~DFLock();
private:
  pthread_mutex_t * mutex_;
  DFMutex * df_mutex_;
};

#endif //DFLOCK_H

