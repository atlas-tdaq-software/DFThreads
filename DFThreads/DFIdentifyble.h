/*--------------------------------------
   Threads library for DataFlow applications

   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFIDENTIFYBLE_H
#define DFIDENTIFYBLE_H
#include <string>


class DFIdentifyble {
public:
  std::string & getName() ;

protected:
  void setName(const char * name) ;

private:
  std::string name_;
};

#endif //DFIDENTIFYBLE_H
