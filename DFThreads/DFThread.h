// -*- c++ -*-
/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: Wrapper for pthread library
   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFTHREAD_H
#define DFTHREAD_H

#include <pthread.h>
#include <sched.h>
#include <exception>
#include <stdlib.h>
#include "DFThreads/DFException.h"

/**
 * Provide the general infrastructure to implement thread classes.
 * Concrete threads shall extend DFThread and provide an implementation for
 * run() and cleanup() abstract methods.
 */
class DFThread {
public:
  class Timeout : public DFException {
  public:
    Timeout() : DFException("This operation timed out") { }
  };

  //BEWARE: the order of the Condition elements MUST reflect the order in
  // the different states are reached by the thread
  enum Condition
    { UNSTARTED, RUNNING, CANCEL_REQUESTED, CLEANING_UP, TERMINATED };

  DFThread();
  virtual ~DFThread();

  /**
   * Create a physical thread which will execute the code of the run() method
   * implementation.
   */
  int startExecution();

  /** Cancel the physical thread associated to this DFThread instance. */
  void stopExecution();

  /**
   * Get running condition of the physical thread associated to a DFThread
   * instance (see Condition).
   */
  Condition getCondition();

  /**
   * Wait fo the physical thread associated to a DFThread instance to reach a
   * defined running condition (see Condition). The method will return even if
   * the thread has passed the specified condition and is now in a successive
   * one (e.g. waitForCondition(RUNNING) will return even
   * if the thread is found in CLEANING_UP state).
   */
  void waitForCondition(Condition condition);

  /**
   * Wait fo the physical thread associated to a DFThread instance to reach a
   * defined running condition (see Condition). The method will return even if
   * the thread has passed the specified condition and is now in a successive
   * one (e.g. waitForCondition(RUNNING) will return even
   * if the thread is found in CLEANING_UP state). Throws Timeout if the
   * required condition is not reached in the specified number of seconds.
   */
  void waitForCondition(Condition condition, long int timeout); //  throw(Timeout)


  /**
   * Get the unique Id of the executing thread
   */
  static unsigned long int id() ;

  /**
   * Get the unique Id of the thread associated with this DFThread instance
   */
  unsigned long int specificId() ;

  /**
   * Give back context to the scheduler
   */
  static void yield() ;
  
  /**
   * Give back context to the scheduler after having tested for 
   * possible cancellation
   */
  static void yieldOrCancel() ;
  
  /**
   * type of handler of uncaught exceptions thrown in the run() method
   */
  typedef void (*exception_handler) (std::exception &e);
  
  /**
   * Set an handler of uncaught exceptions thrown in the run() method
   * It returns a pointer to the previous handler.
   * There can only be 1 handler per all threads.
   */
  static exception_handler set_exception_handler (exception_handler handler);
  
  /**
   * Check for pending cancellation requests for the physical thread. This
   * method shall be called inside subclass implementations of the run()
   * abstract method.
   */
  static void cancellationPoint();

  typedef pthread_key_t Key;

  /**
   * Create a key that can be used to identify and retrieve 
   * thread-specific objects. 
   */
  static Key createKey(void (*destructor)(void *value)=free) ;

  /*
   * Associate an object to a Key for the running thread.  
   */
  static void setSpecificObject(Key &key,const void *object) ;

  /*
   * Retrieve the pointer to the thread-specific object associated
   * to the specified Key.
   */
  static void * getSpecificObject(Key &key) ;

protected:

  /** Code which will be executed at cancellation/termination of the physical
   * threads associated to DFThread subclass instances. */
  virtual void cleanup() = 0;

  /**
   * Code which will be executed by the physical threads associated to the
   * DFThread subclass instances.
   */
  virtual void run() = 0;

private:

  pthread_t id_;
  pthread_attr_t attributes_;
  pthread_mutex_t mutex_;
  pthread_mutex_t cond_mutex_;
  pthread_cond_t cond_;
  Condition condition_;
  int threadsWaiting_;
  static void Cleanup(void *);
  static void * EntryPoint(void * pthis);
  void setCondition(Condition condition);

  static exception_handler handler_ ;
  static void default_exception_handler( std::exception &e ) ;
};

inline DFThread::Condition DFThread::getCondition() {
  return condition_;
}

inline unsigned long int DFThread::id() {
  return pthread_self() ;
}

inline unsigned long int DFThread::specificId() {
  return id_ ;
}

inline void DFThread::yield() {
  sched_yield() ;
}

inline void DFThread::yieldOrCancel() {
  pthread_testcancel(); //The yield should be a cancellation point but is not in pthreads!
  sched_yield() ;
}

inline void DFThread::cancellationPoint() {
  pthread_testcancel();
}

#endif //DFTHREAD_H

