/*--------------------------------------
   Threads library for DataFlow applications
   Purpose: Wrapper for the pthread mutex
            library
   Authors: G. Lehmann, B. Gorini
   Version 1.0 - 23 August 2001
----------------------------------------*/

#ifndef DFMUTEX_H
#define DFMUTEX_H

#include <pthread.h>

#include "DFThreads/DFCreator.h"
#include "DFThreads/DFIdentifyble.h"
#include "DFThreads/DFException.h"

/**
 * Provides the mutex (MUTual EXclusion) synchronization mechanism. The lock
 * on a DFMutex instance is granted to only one thread at a time. Threads
 * requesting to lock a DFMutex which is already locked by another thread are
 * blocked with no CPU consumption until the lock is released. DFMutex
 * implementation is based on the standard pthread mutex but adds to it some
 * checks on possible incorrect operations (see lock( and unlock() methods
 * description) which are not mandatory for pthread specifications.
 */
class DFMutex : public DFIdentifyble {
public:
  class AlreadyLockedByThread : public DFException {
  public:
    AlreadyLockedByThread() :
       DFException("The internal mutex is already locked by this thread") { }
  };

  class NotLockedByThread : public DFException {
  public:
    NotLockedByThread() : DFException("The internal mutex is unlocked or is locked by another thread") {
    }
  };

  /**
   * Returns the instance of DFMutex associated to name. If such an instance
   * already exists returns a pointer to it otherwise creates a new instance,
   * associates it to name an returns the pointer to it. The creation
   * mechanism guarantees that all the threads calling Create(name) with the
   * same name will get a pointer to the same instance, no matter the actual
   * order of calls. The number of active references to a specific instance is
   * counted to guarantee that the instance is deleted only when all the
   * referring threads have called the destroy() method on it.
   */
  static DFMutex * Create(char * Name);

  /** Creates a new anonymous instance of DFMutex. */
  static DFMutex * Create();

  /**
   * Release the reference to the DFMutex instance and delete it only if no
   * other thread is referencing it.
   */
  int destroy();

  /**
   * Releases the lock on the DFMutex.  Throws NotLockedByThread exception
   * if the calling thread is not the one which owns the lock on the DFMutex or if the mutex is unlocked.
   */
  void unlock(); // throw( NotLockedByThread )

  /**
   * Requests to obtain the lock on the DFMutex. The method blocks the thread
   * execution with no CPU consumption as long as the DFMutex is locked by
   * another thread. Throws AlreadyLockedByThread exception if called by a thread which is
   * already locking the mutex, and do not perform the lock request which
   * would otherwise result in a deadlock situation.
   */
  void lock(); // throw( AlreadyLockedByThread )

  /**
   * Requests to obtain the lock on the DFFastMutex. The method return
   * immediately. The return value specifies if the mutex was actually
   * locked or not.
   */
  bool trylock(); //  throw( AlreadyLockedByThread )

  virtual ~DFMutex();
  friend class DFCreator < DFMutex >;
private:
  pthread_t locker_;
  pthread_mutex_t mutex_;
protected:
  DFMutex();
};

#endif //DFMUTEX_H

