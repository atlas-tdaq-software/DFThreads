/*
    DFThread package
    Class: DFException
    Authors: B.Gorini, G.Lehmann
*/

#ifndef DFEXCEPTION_H
#define DFEXCEPTION_H

#include <exception>

/** Base class for exceptions. */
class DFException : public std::exception {
public:
  /** Provides a textual description of the cause of the exception. */
  const char * what() const noexcept;
private:
  const char * m_text;
protected:
  DFException(const char * text) ;
};
#endif //DFEXCEPTION_H
